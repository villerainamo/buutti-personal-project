### Description:
The aim of this project is to create a scalable application for a non-profit organization. The application will work as an educational resource and information portal with both non-user and user spaces. 

### Contents:
The application will hold scheduling information regarding upcoming clubs, events, and activities held by the non-profit organization. The application will also hold distance-learning materials related to the science education themes and activities. 

### Functionalities:
Non-registered users of the application will be able to see current schedules and join scheduled activities by submitting their information. Registered users will be able to additionally see and access distance-learning courses and take part in puzzles, submit questions related to science education, and post on the message board. 

Back-End functionalities will be simulated with JSON-server and the possibility of linking a more developed Back-End will be included. 

### Framework & Structure:
 The application will use React and JSON-server in the initial configuration. The structure of the project will consist of pages, components, and functions listed in their respective sub-directories. 

### Technologies:
 React, JavaScript, JSON-server

## Development:
### Basic structure, components, and functions 
* Main page structure and placeholder content 
* Schedule page structure and placeholder content 
* Course page structure and placeholder content 
* Messaging board structure 
* Routing 

### Basic functionalities 
* Header navigation 
* Main page feeds 
* Course page feeds 
* Schedule page template 
* Login / Signup

### Refine components 
* Refined main page feed component items 
* Refined course page feed component items 
* Refined schedule page component items 
* Refined routing 

### Advanced functionalities 
* Course content and related user functions (quiz-answering) 
* User profile page and editing 
* Login & Signup full functionality 
* Message board basic functionality

## Implemented:
- Base layout for all pages
- Basic internal navigation
- Basic UI functionality
- Schedule calendar event display
- Message board basic messaging
- Front page API-activity feed

## Unfinished / Not-Implemented:
- User functions
- Login / Signup
- Message board message deletion
- Front page activity feed manipulation
- Join event functionality

import React from 'react'
import { createContext } from 'react'
import { useState, useEffect } from 'react'
import axios from 'axios'

const UserContext = createContext({})

export const UserProvider = (props) => {

    const { children } = props
    const [user, setUser] = useState({})

    useEffect(() => {
        const getData = async () => {
            const data = await axios.get(`http://localhost:5001/users/${1}`)
            setUser(data.data)
            console.log(user)
        }
        getData()
    }, [])

/*    
    const [users, setUsers] = useState({})

    useEffect(() => {
        const getData = async () => {
            const data = await axios.get(`http://localhost:5001/users/`)
            console.log(data)
            setUsers(data.data)
        }
        getData()
    }, [])

*/

    return (
        <UserContext.Provider value={{ user }}>
            {children}
        </UserContext.Provider>
    )
}

export default UserContext


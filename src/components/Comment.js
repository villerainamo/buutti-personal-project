import { Card, Typography } from '@mui/material'
import React from 'react'

const Comment = ( message, user ) => {
    return (
        <div className='comment'>
            <Card sx={{ height: "80%", width: "75%", pt: 2 }}>
                <div className="comment-message">
                    <Typography variant="body1" mb="5px" >Kommentti: {message.message}</Typography></div>
                <div className="comment-user">
                    <Typography variant="body1" mb="5px">Käyttäjä: {message.user}</Typography></div>                
            </Card>
        </div>
    )
}

export default Comment

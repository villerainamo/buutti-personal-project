import { Button, Card, Typography } from '@mui/material'
import { useState, useEffect } from 'react'
import React from 'react'
import ScheduleEventDetails from './ScheduleEventDetails'

const ScheduleEvent = ( { event, day, handleSelect } ) => {

    return (
        <Card sx={{ height: 150, width: 150, m: 2, p: 1 }} onClick={handleSelect}>
            <Typography variant="h5" component="h5" fontSize={18}>
            <div className="schedule-event" >
                <div className="calendar-day"><h4>{day}.</h4></div>
                <div className="calendar-day-event"><p>Päivä</p></div>
                <ScheduleEventDetails 
                        event={event}/>       
            </div>
            </Typography>
        </Card>
    )
}

export default ScheduleEvent

import React from 'react'
import Button from "@mui/material/Button"
import { Stack, Typography } from '@mui/material'

const Header = () => {
    return (
        <div className="header-bar">
            <div className="header-title">
                <Typography variant="button" component="h3" fontSize={32}>
                    Keneraattori
                </Typography>
            </div>  
            <div className="nav-bar">
                <Stack spacing={1} direction="row">
                <Button variant="contained" ><a href="/">Etusivu</a></Button>
                <Button variant="contained" ><a href="/schedule">Aikataulu</a></Button>
                <Button variant="contained" ><a href="/courses">Etäkerhot</a></Button>
                <Button variant="contained" ><a href="/messageboard">Keskustelupalsta</a></Button>
                <Button variant="contained" ><a href="/profile">Profiili</a></Button>
                </Stack>
            </div>
        </div>
    )
}

export default Header

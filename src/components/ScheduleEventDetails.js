import { Typography } from '@mui/material'
import React from 'react'

const ScheduleEventDetails = ({ event }) => {

    return (
        <div className="schedule-event-detail">
            <Typography variant="p" fontSize={18}>
            <p>{event.event}</p>
            <p>{event.location}</p>
            </Typography>
        </div>
    )
}

export default ScheduleEventDetails

import React from 'react'
import ActivityItem from './ActivityItem'
import Paper from "@mui/material/Paper"
import axios from 'axios'
import { useState, useEffect } from 'react'

const ActivityFeed = () => {
/*
    const options = {
        method: 'GET',
        url: 'https://www.boredapi.com/api/activity/',
      };

    axios.request(options).then(function (response) {
	    console.log(response.data);
    }).catch(function (error) {
	    console.error(error);
    });
    
*/

    const [activity1, setActivity1] = useState([])
    const [activity2, setActivity2] = useState([])
    const [activity3, setActivity3] = useState([])

    useEffect(() => {
        const getData = async () => {
            const data = await axios.get(`https://www.boredapi.com/api/activity/`)
            setActivity1(data.data)
        }
        getData()
    }, [])

    useEffect(() => {
        const getData = async () => {
            const data = await axios.get(`https://www.boredapi.com/api/activity/`)
            setActivity2(data.data)
        }
        getData()
    }, [])

    useEffect(() => {
        const getData = async () => {
            const data = await axios.get(`https://www.boredapi.com/api/activity/`)
            setActivity3(data.data)
        }
        getData()
    }, [])



    return (
        <Paper sx={{ height: 1300 }}>   
        <div className="activity-container">
            <Paper elevation={3} sx={{ height: 1200 }}>
                <ActivityItem activity={activity1}/>
                <ActivityItem activity={activity2}/>
                <ActivityItem activity={activity3}/>
            </Paper>
        </div></Paper> 
    )
}

export default ActivityFeed

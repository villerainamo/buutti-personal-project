import React from 'react'
import Card from "@mui/material/Card"
import { Typography } from '@mui/material'

const ActivityItem = ({ activity }) => {

    console.log(activity)

    return (
        <div className="activity-item">
            <Card sx={{ height: 300, width: "100%" }}>
                <Typography variant="h3" component="h4">
                <div className="activity-label"><h4>Aktiviteetti:</h4></div>
                <div className="activity-description"><p>{activity.activity}</p></div>
                <div className="activity-label"><h4>Helppous:</h4></div>
                <div className="activity-description"><p>{activity.accessibility}</p></div>
                <div className="activity-label"><h4>Kustannus:</h4></div>
                <div className="activity-description"><p>{activity.price}</p></div>
                <div className="activity-label"><h4>Linkki:</h4></div>
                <div className="activity-description"><p>{activity.link}</p></div>
                <div className="activity-label"><h4>Tyyppi:</h4></div>
                <div className="activity-description"><p>{activity.type}</p></div>
                <div className="activity-label"><h4>Osallistujat:</h4></div>
                <div className="activity-description"><p>{activity.participants}</p></div>
                </Typography>
            </Card>
        </div>
        
    )
}

export default ActivityItem

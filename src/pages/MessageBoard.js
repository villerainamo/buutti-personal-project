import React from 'react'
import Comment from '../components/Comment'
import { useState, useEffect } from 'react'
import axios from 'axios'
import { Button, Card, Input, Paper } from '@mui/material'

const MessageBoard = () => {

    const [messages, getMessages] = useState([])
    const [newMessage, setNewMessage] = useState({
        message:"",
        user:""
    })

    useEffect(() => {
        const getData = async () => {
            const data = await axios.get("http://localhost:5001/message-board-messages")
            getMessages(data.data)
        }
        getData()
    }, [])

    const postMessage = async (event) => {
        event.preventDefault()
        const res = await axios.post("http://localhost:5001/message-board-messages", newMessage)
        const data = res.data
        console.log(data)
            const getData = async () => {
                const data = await axios.get("http://localhost:5001/message-board-messages")
                getMessages(data.data)
            }
            getData()
        }

    return (
        <div className='message-board-container'>
            <Paper>
            {messages.map((element) =>
                <Comment
                    key={element.id}
                    message={element.message}
                    user={element.user}
                />    
            )}
            <Card sx={{ width: 500, height: 400, m: 2 }}>
                <div className="add-comment">
                    <form onSubmit={postMessage}>
                        <div className="add-comment-field">
                            <Input
                                placeholder="Kommentti" 
                                value={newMessage.message}
                                onChange={(e) => setNewMessage({...newMessage, message: e.target.value})} />
                            </div>
                        <div className="add-comment-user-field">
                            <Input
                                placeholder="Käyttäjä" 
                                value={newMessage.user}
                                onChange={(e) => setNewMessage({...newMessage, user: e.target.value})} />
                            </div>
                        <div className="leave-comment-button">
                        <Button type="submit" sx={{ mt: 2, width: "80%", height: 50 }}>Lisää kommentti</Button>
                        </div>    
                    </form>
                </div>
            </Card>
            </Paper>
        </div>
    )
}

export default MessageBoard

import { Button, Card } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import React from 'react'

const Course = ( course, description ) => {

    const navigate = useNavigate();

    const joinCourse = () => {
        navigate("/coursecontent", { state: course })
    }

    return (
        <div className='course-container'>
        <Card sx={{ padding: 2, width: 300 }}>
            <div className='course-title'><h3>{course.course}</h3></div>
            <div className='course-description'><p>{course.description}</p></div>
            <Button onClick={joinCourse}>Etäkerhoon</Button>
        </Card>
        </div>
    )
}

export default Course

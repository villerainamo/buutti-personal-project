import React from 'react'
import ActivityFeed from '../components/ActivityFeed'
import Paper from "@mui/material/Paper"

const Main = () => {
    return (
        <Paper elevation={3}>
        <div className="main-container">
            <ActivityFeed />
        </div></Paper>
    )
}

export default Main

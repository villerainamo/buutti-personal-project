import { Button, Card, Paper } from '@mui/material'
import { useLocation } from 'react-router-dom'
import React from 'react'

const CourseContent = () => {

    const { state } = useLocation();

    const course = state;
    
    const handleClick = () => {
        console.log(course)
    }

    return (
        <div className='course-content-container'>
            <Paper>
                <Paper>
                    <div className="course-content-material">
                        <p>Lorem ipsum lorem ipsum</p>
                        <h3>{course.course}</h3>
                        <p>{course.description}</p>
                        <p>{course.content}</p>
                    </div>
                </Paper>
                <Card>
                    <div className="course-content-question">
                        <p>Lorem ipsum lorem ipsum</p>
                    </div>
                </Card>
                <Button onClick={handleClick}>Testaa</Button>
            </Paper>
        </div>
    )
}

export default CourseContent

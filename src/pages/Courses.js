import React from 'react'
import Paginations from "@mui/material/Pagination/Pagination.js";
import PropTypes from "prop-types"
import { useState, useEffect} from 'react'
import axios from 'axios'
import Course from './Course'

const Courses = () => {

    const [courses, getCourses] = useState([])

    useEffect(() =>{
        const getData = async () => {
            const data = await axios.get("http://localhost:5001/courses")
            getCourses(data.data)
        }
        getData()
    }, [])

    console.log(courses)

    return (
        <>
        <div className="courses-container">
            {courses.map((element) => 
                <Course
                    key={element.id} 
                    course={element.course}
                    description={element.description}                
                /> 
            )}
        </div>
        <div className='pagination-bar'><Paginations
            pages={[
                { text: "PREV" },
                { text: 1 },
                { text: 2 },
                { active: true, text: 3 },
                { text: 4 },
                { text: 5 },
                { text: "NEXT" }
            ]}
            color="info"
        /></div>
        </>
    )
    
}

export default Courses



import React from 'react'
import Card from "@mui/material/Card"
import UserContext from '../context/UserContext'
import { useContext } from 'react'
import { Button, Paper } from '@mui/material'

const UserProfile = () => {

    const {user} = useContext(UserContext)

    const testClick = () => {
        console.log(user)
    }    

    return (
        <div className='profile-container'>
            <Paper>
            <Card sx={{ 
                height: 300, 
                width: 600, 
                padding: 2 
            }}>
                <p>Lorem ipsum Lorem ipsum Lorem ipsum</p>
            {user != undefined ? <div>
                <h5>{user.user}</h5>
                <p>{user.email}</p></div> 
                : "Loading "}
                <div><Button onClick={testClick}>Testaa</Button></div>
            </Card>
            </Paper>
        </div>
    )
}

export default UserProfile

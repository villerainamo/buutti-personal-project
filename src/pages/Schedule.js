import React from 'react'
import { useState, useEffect } from 'react'
import axios from 'axios'
import ScheduleEvent from '../components/ScheduleEvent'
import { Button, Paper } from '@mui/material'

const Schedule = () => {

    const [month, setMonth] = useState()
    const [events, getEvents] = useState()
    const [selectedDay, setSelectedDay] = useState("false")

    const handleSelect = () => {
        setSelectedDay(!selectedDay)
        console.log(selectedDay)
    }

    useEffect(() => {
        const daysInMonth = () => {
            const now = new Date();
            return new Array(new Date(now.getFullYear(), now.getMonth()+1, 0).getDate()) 
        }
        setMonth(Array.from(daysInMonth().keys()))
    },[])

    useEffect(() => {
        const getData = async () => {
            const data = await axios.get("http://localhost:5001/events")
            getEvents(data.data)
        }
        getData()
    }, [])

    return (
        <div className="schedule">
            <Paper sx={{ 
                width: "100%", 
                height: "100%"}}>
            <div className="schedule-container">
                {month != undefined && events != undefined
                    ? (month.map((element) => 
                        <ScheduleEvent
                            day={element + 1} 
                            key={element}
                            event={events[element]} 
                            handleSelect={handleSelect}/>
                    ))
                    : "Loading"
                }
                <Button>Aseta kuukausi</Button>               
            </div>
            </Paper>
        </div>
    )
}

export default Schedule

import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Footer from './components/Footer';
import Header from './components/Header';
import Main from './pages/Main';
import Schedule from './pages/Schedule';
import Courses from './pages/Courses';
import UserProfile from './pages/UserProfile';
import MessageBoard from './pages/MessageBoard';
import CourseContent from './pages/CourseContent';
import { UserProvider } from './context/UserContext';

function App() {
  return (
    <BrowserRouter>
    <UserProvider>
      <Header />
        <Routes>
          <Route exact path='/' element={<Main />} />
          <Route exact path='/schedule' element={<Schedule />} />
          <Route exact path='/courses' element={<Courses />} />
          <Route exact path='/profile' element={<UserProfile />} />
          <Route exact path='/messageboard' element={<MessageBoard />} />
          <Route exact path='/coursecontent' element={<CourseContent />} />
        </Routes>
      <Footer />
    </UserProvider>
    </BrowserRouter>
  );
}

export default App;
